function pairs(obj) {
    if (typeof (obj) != 'object') {
        return []
    }
    let pairs_arr = []
    for (const property in obj) {
        pairs_arr.push([property, obj[property]])

    }
    return pairs_arr;
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}
module.exports = pairs;