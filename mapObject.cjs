
function mapObject(obj, cb) {

    if (typeof (obj) != 'object') {
        return []
    }
    for (const property in obj) {
        obj[property] = cb(obj[property], property)
    }
    return obj
}
module.exports = mapObject