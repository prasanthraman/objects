function invert(obj) {
    if (typeof (obj) != 'object') {
        return []
    }
    let newObj = {}
    for (const property in obj) {
        newObj[obj[property]] = property
    }
    return newObj;
}
module.exports = invert;
