function keys(obj) {
    let keys_arr = []
    if (typeof (obj) != 'object') {
        return []
    }
    for (const property in obj) {
        keys_arr.push(property)
    }
    return keys_arr;
}
module.exports = keys;