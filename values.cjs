
function values(obj) {
    let values_arr = []
    if (typeof (obj) != 'object') {
        return []
    }
    for (const property in obj) {
        if (typeof (obj[property]) == 'function') {
            //ignore
        } else {
            values_arr.push(obj[property])
        }
    }
    return values_arr;


}
module.exports = values;
